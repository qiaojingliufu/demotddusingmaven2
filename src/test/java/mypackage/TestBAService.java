package mypackage;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class TestBAService {

	@Mock 
	BARestClient restClient;
	
	@Mock 
	UI ui;

	@InjectMocks
	BAService fixture;
	
	BankAccount ba123, ba456;
	
	@Before
	public void setup() {
		ba123 = new BankAccount("David");
		ba123.setId(123);
		ba123.deposit(1000);

		ba456 = new BankAccount("Sarah");
		ba456.setId(456);
		ba456.deposit(2000);
	}
	
	@Test
	public void depositIntoAccount_existingAccount_increasesBalance() {

		// Arrange: Set expectations on mock objects.
		when(ui.promptForInteger("Account id")).thenReturn(123);
		when(restClient.getById(123)).thenReturn(ba123);
		when(ui.promptForInteger("Amount to deposit")).thenReturn(50);
		when(restClient.update(123, ba123)).thenReturn(true);
	
		// Act.
		fixture.depositIntoAccount();
		
		// Assert.
		assertThat(ba123.getBalance(), equalTo(1050));
		
		// Verify expected methods were invoked on the mock objects.
		verify(restClient).getById(123);
		verify(ui, times(2)).promptForInteger(anyString());
		verify(restClient).update(123, ba123);    	
	}

	@Test(expected=IllegalArgumentException.class)
	public void depositIntoAccount_nonExistingAccount_exceptionOccurs() {

		// Arrange: Set expectations on mock object.
		when(restClient.getById(123)).thenReturn(null);
	
		// Act.
		fixture.depositIntoAccount();
	}

	@Test
	public void withdrawFromAccount_existingAccount_decreasesBalance() {

		// Arrange: Set expectations on mock objects.
		when(ui.promptForInteger("Account id")).thenReturn(123);
		when(ui.promptForInteger("Amount to withdraw")).thenReturn(50);
		when(restClient.getById(123)).thenReturn(ba123);
		when(restClient.update(123, ba123)).thenReturn(true);
	
		// Act.
		fixture.withdrawFromAccount();
		
		// Assert.
		assertThat(ba123.getBalance(), equalTo(950));
		
		// Verify expected methods were invoked on the mock objects.
		verify(restClient).getById(123);
		verify(ui, times(2)).promptForInteger(anyString());
		verify(restClient).update(123, ba123);    	
	}

	@Test(expected=IllegalArgumentException.class)
	public void withdrawFromAccount_nonExistingAccount_exceptionOccurs() {

		// Arrange: Set expectations on mock object.
		when(restClient.getById(123)).thenReturn(null);
	
		// Act.
		fixture.withdrawFromAccount();
	}

	@Test
	public void transferFunds_existingAccounts_amountTransferred() {

		// Arrange: Set expectations on mock objects.
		when(ui.promptForInteger("Account id #1")).thenReturn(123);
		when(ui.promptForInteger("Account id #2")).thenReturn(456);
		when(ui.promptForInteger("Amount to transfer")).thenReturn(50);
		when(restClient.getById(123)).thenReturn(ba123);
		when(restClient.getById(456)).thenReturn(ba456);
		when(restClient.update(123, ba123)).thenReturn(true);
		when(restClient.update(456, ba456)).thenReturn(true);
	
		// Act.
		fixture.transferFunds();
		
		// Assert.
		assertThat(ba123.getBalance(), equalTo(950));
		assertThat(ba456.getBalance(), equalTo(2050));
		
		// Verify expected methods were invoked on the mock objects.
		verify(restClient, times(2)).getById(anyInt());
		verify(ui, times(3)).promptForInteger(anyString());
		verify(restClient, times(2)).update(anyInt(), anyObject());    	
	}

}
